package ausweis.cofinpro.de.ausweis_app;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;

/**
 * @author Gregor Tudan, Cofinpro AG
 */
public class AusweisAppDialog extends DialogFragment {
    private static final int AUSWEIS2APP_REQUEST_CODE = 1;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.install_ausweis_app)
                .setPositiveButton(R.string.install, (dialog, id) ->
                        installAusweis2App()
                ).setNegativeButton(R.string.cancel, (dialog, id) -> {
            // User cancelled the dialog
        });
        // Create the AlertDialog object and return it
        return builder.create();
    }

    private void installAusweis2App() {
        final String name = "com.governikus.ausweisapp2";
        try {
            Log.v("ausweis", "Sending intent to install " + name);
            this.startActivityForResult(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + name)), AUSWEIS2APP_REQUEST_CODE);
        } catch (ActivityNotFoundException e) {
            // Use the browser if Play Store is not installed, too!
            Log.v("ausweis", "Play store not found, sending intent to rediret user to play.google.com", e);
            this.startActivityForResult(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + name)), 1);
        }
    }
}
