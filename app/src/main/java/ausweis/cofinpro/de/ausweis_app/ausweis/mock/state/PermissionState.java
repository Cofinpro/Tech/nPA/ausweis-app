package ausweis.cofinpro.de.ausweis_app.ausweis.mock.state;

import android.os.RemoteException;
import android.util.Log;
import ausweis.cofinpro.de.ausweis_app.ausweis.mock.EidSession;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Collections;

/**
 * @author Gregor Tudan, Cofinpro AG
 */
public class PermissionState implements EidState {

    private EidSession eidSession;
    private boolean accepted;

    PermissionState(EidSession eidSession) {
        Log.v("AusweisMock", "Reached state " + getClass().getName());
        this.eidSession = eidSession;
    }

    @Override
    public void receivedNFC() throws RemoteException {
        if (this.accepted) {
            next();
        }
    }

    @Override
    public void accept() throws RemoteException {
        this.accepted = true;
        if (this.eidSession.getTag() != null) {
            next();
        }
    }

    private void next() throws RemoteException {
        AuthenticateState eidState = new AuthenticateState(this.eidSession);
        this.eidSession.setEidState(eidState);
        eidState.requestPIN(null);
    }

    void requestPermissions() throws RemoteException, JSONException {
        JSONObject requestedRights = new JSONObject()
                .put("effective", new JSONArray(Arrays.asList("FamilyName", "GivenNames", "DocumentType")))
                .put("optional", new JSONArray(Collections.singletonList("GivenNames")))
                .put("required", new JSONArray(Arrays.asList("FamilyName", "DocumentType")));

        this.eidSession.returnMessage("ACCESS_RIGHTS", Collections.singletonMap("chat", requestedRights));
    }
}
