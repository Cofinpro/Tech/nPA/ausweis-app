package ausweis.cofinpro.de.ausweis_app.ausweis;

import java.util.Objects;

public class Permission {

    private final String name;
    private boolean required;
    private boolean granted;

    private Permission(String name, boolean required) {
        this.name = name;
        this.required = required;
        this.granted = required;
    }

    public static Permission of(String name) {
        return new Permission(name, false);
    }

    public String getName() {
        return name;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public boolean isGranted() {
        return granted;
    }

    public void setGranted(boolean granted) {
        this.granted = granted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Permission that = (Permission) o;
        return required == that.required &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, required);
    }

    @Override
    public String toString() {
        return "Permission{" +
                "name='" + name + '\'' +
                ", required=" + required +
                ", granted=" + granted +
                '}';
    }
}
