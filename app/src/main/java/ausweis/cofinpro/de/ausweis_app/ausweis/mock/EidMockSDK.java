package ausweis.cofinpro.de.ausweis_app.ausweis.mock;

import android.nfc.Tag;
import android.os.RemoteException;
import android.util.Log;
import com.governikus.ausweisapp2.IAusweisApp2Sdk;
import com.governikus.ausweisapp2.IAusweisApp2SdkCallback;



import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Gregor Tudan, Cofinpro AG
 */
public class EidMockSDK extends IAusweisApp2Sdk.Stub {

    private IAusweisApp2SdkCallback callback;
    private ConcurrentHashMap<String, EidSession> sessions = new ConcurrentHashMap<>();

    @Override
    public boolean connectSdk(IAusweisApp2SdkCallback pCallback) {
        this.callback = pCallback;
        return true;
    }

    @Override
    public boolean send(String pSessionId, String pMessageFromClient) throws RemoteException {
        Objects.requireNonNull(pSessionId, "Session ID must not be null");
        Log.d("AusweisMock", "Got message " + pMessageFromClient);

        try {
            JSONObject jsonObject = new JSONObject(pMessageFromClient);
            String cmd = jsonObject.getString("cmd");
            switch (cmd) {
                case "AUTH":
                    EidSession session = new EidSession(callback);
                    sessions.put(pSessionId, session);
                    session.initialize();
                    break;
                case "ACCEPT":
                    sessions.get(pSessionId).getEidState().accept();
                    break;
                case "SET_PIN":
                    sessions.get(pSessionId).getEidState().setPIN(jsonObject.getString("value"));
                    break;
                case "SET_PUK":
                    sessions.get(pSessionId).getEidState().setPUK(jsonObject.getString("value"));
                    break;
                case "SET_CAN":
                    sessions.get(pSessionId).getEidState().setCAN(jsonObject.getString("value"));
                    break;
                default:
            }

            if (sessions.get(pSessionId).isClosed()) {
                Log.v("AusweisMock", "Closing session " + pSessionId);
                sessions.remove(pSessionId);
            }
            return true;

        } catch (JSONException e) {
            Log.e("AusweisMock", "Failed generate response", e);
            return false;
        }
    }

    @Override
    public boolean updateNfcTag(String pSessionId, Tag pTag) throws RemoteException {
        this.sessions.get(pSessionId).setTag(pTag);
        return true;
    }


}
