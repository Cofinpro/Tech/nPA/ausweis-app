package ausweis.cofinpro.de.ausweis_app.ausweis.mock;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * @author Gregor Tudan, Cofinpro AG
 */
public class EidMockService extends Service {

    private final EidMockSDK sdk = new EidMockSDK();
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return sdk;
    }
}
