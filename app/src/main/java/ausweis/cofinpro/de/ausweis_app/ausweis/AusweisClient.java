package ausweis.cofinpro.de.ausweis_app.ausweis;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.util.Log;

import com.governikus.ausweisapp2.IAusweisApp2Sdk;
import com.governikus.ausweisapp2.IAusweisApp2SdkCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import ausweis.cofinpro.de.ausweis_app.MainActivity;
import ausweis.cofinpro.de.ausweis_app.ausweis.mock.EidMockService;

/**
 * @author Gregor Tudan, Cofinpro AG
 */
public class AusweisClient {
    public static final String SHA_256 = "SHA256";
    private static final String COMMAND_ACCEPT = "ACCEPT";
    private static final String MESSAGE_ACCESS_RIGHTS = "ACCESS_RIGHTS";
    private static final String MESSAGE_ENTER_PIN = "ENTER_PIN";
    private static final String MESSAGE_ENTER_CAN = "ENTER_CAN";
    private static final String MESSAGE_ENTER_PUK = "ENTER_PUK";
    private static final String PERMISSIONS_EFFECTIVE = "effective";
    private static final String PERMISSIONS_REQUIRED = "required";
    private static final String CHAT = "chat";
    private static final String MSG = "msg";

    private MainActivity mainActivity;
    private PackageManager packageManager;

    private LocalCallback mCallback = new LocalCallback();
    private IAusweisApp2Sdk mSdk;

    public void handleIntent(Intent intent) {
        final Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        if (tag != null) {
            try {
                mSdk.updateNfcTag(mCallback.mSessionID, tag);
            } catch (RemoteException e) {
                Log.e("ausweis", e.getMessage(), e);
            }
        }
    }

    public void setPIN(String pin) {
        try {
            sendCommand("SET_PIN", Collections.singletonMap("value", pin));
        } catch (JSONException | RemoteException e) {
            Log.e("ausweis", e.getMessage(), e);
        }
    }

    public void setPUK(String puk) {
        try {
            sendCommand("SET_PUK", Collections.singletonMap("value", puk));
        } catch (JSONException | RemoteException e) {
            Log.e("ausweis", e.getMessage(), e);
        }
    }

    public void setCAN(String can) {
        try {
            sendCommand("SET_CAN", Collections.singletonMap("value", can));
        } catch (JSONException | RemoteException e) {
            Log.e("ausweis", e.getMessage(), e);
        }
    }

    public boolean accept() {
        try {
            return sendCommand(COMMAND_ACCEPT, Collections.emptyMap());
        } catch (JSONException | RemoteException e) {
            Log.e("ausweis", e.getMessage(), e);
            return false;
        }
    }

    public boolean accept(Map<String, String> payload) throws JSONException, RemoteException {
        return sendCommand(COMMAND_ACCEPT, payload);
    }

    class LocalCallback extends IAusweisApp2SdkCallback.Stub {
        String mSessionID = UUID.randomUUID().toString();

        @Override
        public void sessionIdGenerated(
                String pSessionId, boolean pIsSecureSessionId) throws RemoteException {
            mSessionID = pSessionId;
        }

        @Override
        public void receive(String pJson) throws RemoteException {
            Log.v("ausweis", "Received message:" + pJson);
            try {
                JSONObject jsonObject = new JSONObject(pJson);
                String message = jsonObject.getString(MSG);
                if (message.equals(MESSAGE_ACCESS_RIGHTS)) {
                    JSONObject chat = jsonObject.getJSONObject(CHAT);
                    List<Permission> requestedPermissions = parsePermissions(chat);
                    mainActivity.showPermissionsList(requestedPermissions);
                } else if (message.equals(MESSAGE_ENTER_PIN)) {
                    mainActivity.showPinView(jsonObject.optString("error", null));
                } else if (message.equals(MESSAGE_ENTER_CAN)) {
                    mainActivity.showCanView(jsonObject.optString("error", null));
                } else if (message.equals(MESSAGE_ENTER_PUK)) {
                    mainActivity.showPukView(jsonObject.optString("error", null));
                } else if (message.equals("AUTH")) {
                    if (jsonObject.has("result")) {
                        JSONObject result = jsonObject.getJSONObject("result");
                        if (result.getString("major").endsWith("#ok")) {
                            mainActivity.showSuccess();
                        } else {
                            mainActivity.showError(result.optString("message", "Unbekannter Fehler"));
                        }
                    } else {
                        Log.w("ausweis", "AUTH message does not contain result element");
                    }
                }
            } catch (Exception e) {
                Log.e("ausweis", e.getMessage(), e);
            }
        }

        @Override
        public void sdkDisconnected() throws RemoteException {

        }
    }

    private List<Permission> parsePermissions(JSONObject chat) throws JSONException {
        Map<String, Permission> permissionMap = new LinkedHashMap<>();

        JSONArray effectivePermissions = chat.getJSONArray(PERMISSIONS_EFFECTIVE);
        for (int i = 0; i < effectivePermissions.length(); i++) {
            String permissionName = effectivePermissions.getString(i);
            permissionMap.put(permissionName, Permission.of(permissionName));
        }

        JSONArray requiredPermissions = chat.getJSONArray(PERMISSIONS_REQUIRED);
        for (int i = 0; i < requiredPermissions.length(); i++) {
            String permissionName = requiredPermissions.getString(i);
            Permission permission = permissionMap.computeIfAbsent(permissionName, Permission::of);
            permission.setRequired(true);
            permission.setGranted(true);
        }

        return new ArrayList<>(permissionMap.values());
    }

    public AusweisClient(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        this.packageManager = mainActivity.getPackageManager();
    }

    public void createConnection() {
        ServiceConnection mConnection = new ServiceConnection() {

            @Override
            public void onServiceConnected(ComponentName className, IBinder service) {
                try {
                    mSdk = IAusweisApp2Sdk.Stub.asInterface(service);

                    if (!mSdk.connectSdk(mCallback)) {
                        // already connected? Handle error...
                        Log.w("ausweis", "Unable to connect");
                    }

                    Log.v("ausweis", "Connected to SDK");

                    sendCommand("AUTH", null);
                    //sendCommand("RUN_AUTH", Collections.singletonMap("tcTokenURL", "https://test.governikus-eid.de/DEMO"));
                    Log.v("ausweis", "Command sent");

                } catch (RemoteException | JSONException e) {
                    // handle exception
                    Log.e("ausweis", e.getMessage(), e);
                }

            }

            @Override
            public void onServiceDisconnected(ComponentName className) {
                mSdk = null;
            }
        };


        Intent serviceIntent;
        //serviceIntent = new Intent("com.governikus.ausweisapp2.START_SERVICE");
        //serviceIntent.setPackage("com.governikus.ausweisapp2");
        serviceIntent = new Intent(mainActivity, EidMockService.class);

        mainActivity.bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE);

    }

    @NonNull
    private Intent getServiceIntent() {
        String name = "com.governikus.ausweisapp2.START_SERVICE";
        Intent serviceIntent = new Intent(name);
        serviceIntent.setPackage("com.governikus.ausweisapp2");
        return serviceIntent;
    }

    boolean sendCommand(String command, Map<String, String> payload) throws JSONException, RemoteException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("cmd", command);

        if (payload != null) {
            for (Map.Entry<String, String> entry : payload.entrySet()) {
                jsonObject.put(entry.getKey(), entry.getValue());
            }
        }

        String commandString = jsonObject.toString();
        Log.v("ausweis", "Command sent: " + commandString);
        return mSdk.send(mCallback.mSessionID, commandString);

    }

    public boolean checkPreconditions() {
        Log.v("ausweis", "Checking preconditions");
        return isAusweisApp2Installed();
    }


    private boolean isAusweisApp2Installed() {
        Intent serviceIntent = getServiceIntent();
        List<ResolveInfo> list = packageManager.queryIntentServices(serviceIntent, PackageManager.MATCH_ALL);
        return list != null && !list.isEmpty();
    }
}
