package ausweis.cofinpro.de.ausweis_app.ausweis.mock.state;

import android.os.Handler;
import android.os.RemoteException;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import ausweis.cofinpro.de.ausweis_app.ausweis.mock.EidSession;

/**
 * @author Gregor Tudan, Cofinpro AG
 */
public class AuthenticateState implements EidState {

    private static final String CORRECT_PIN = "666666";
    private static final String CORRECT_PUK = "666666666";
    private static final String CORRECT_CAN = "666666";

    private EidSession eidSession;

    private int incorrectPinCounter = 3;

    AuthenticateState(EidSession eidSession) {
        Log.v("AusweisMock", "Reached state " + getClass().getName());
        this.eidSession = eidSession;
    }

    @Override
    public void setPIN(String pin) {
        (new Handler()).postDelayed(() -> setPINInternal(pin), 3000);
    }

    private void setPINInternal(String pin) {
        try {
            if (CORRECT_PIN.equals(pin)) {
                finishSuccess();
            } else {
                incorrectPinCounter--;
                if (incorrectPinCounter > 1) {
                    requestPIN("Falsche PIN eingegeben, bitte versuchen Sie es nochmal");
                } else if (incorrectPinCounter == 1) {
                    requestCAN("PIN zu oft falsch eingegeben, bitte mit CAN entsperren");
                } else if (incorrectPinCounter == 0) {
                    requestPUK("PIN zu oft falsch eingegeben, bitte mit PUK entsperren");
                } else {
                    finishError("Falsche PIN, gesperrt");
                }
            }
        } catch (RemoteException e) {
            Log.e("AusweisMock", "RemoteError", e);
        }
    }

    @Override
    public void setPUK(String puk) {
        (new Handler()).postDelayed(() -> setPUKInternal(puk), 3000);
    }

    private void setPUKInternal(String puk) {
        try {
            if (CORRECT_PUK.equals(puk)) {
                requestPIN(null);
            } else {
                finishError("Falsche PUK, gesperrt");
            }
        } catch (RemoteException e) {
            Log.e("AusweisMock", "RemoteError", e);
        }
    }

    @Override
    public void setCAN(String can) throws RemoteException {
        (new Handler()).postDelayed(() -> setCANInternal(can), 3000);
    }

    private void setCANInternal(String can) {
        try {
            if (CORRECT_CAN.equals(can)) {
                requestPIN(null);
            } else {
                finishError("Falsche CAN, gesperrt");
            }
        } catch (RemoteException e) {
            Log.e("AusweisMock", "RemoteError", e);
        }
    }

    void requestPIN(String error) throws RemoteException {
        Map<String, Object> payload = error != null ? Collections.singletonMap("error", error) : Collections.emptyMap();
        eidSession.returnMessage("ENTER_PIN", payload);
    }

    private void requestPUK(String error) throws RemoteException {
        Map<String, Object> payload = error != null ? Collections.singletonMap("error", error) : Collections.emptyMap();
        eidSession.returnMessage("ENTER_PUK", payload);
    }

    private void requestCAN(String error) throws RemoteException {
        Map<String, Object> payload = error != null ? Collections.singletonMap("error", error) : Collections.emptyMap();
        eidSession.returnMessage("ENTER_CAN", payload);
    }

    private void finishSuccess() throws RemoteException {
        Map<String, Object> result = new HashMap<>();
        try {
            result.put("result", new JSONObject().put("major", "http://www.bsi.bund.de/ecard/api/1.1/resultmajor#ok"));
            result.put("url", "https://test.governikus-eid.de/DEMO/?refID=123456");
            this.eidSession.close();
            this.eidSession.returnMessage("AUTH", result);
        } catch (JSONException e) {
            Log.e("AusweisMock", "Failed to generate JSON", e);
        }
    }

    private void finishError(String errorMsg) throws RemoteException {
        Map<String, Object> result = new HashMap<>();
        try {
            result.put("result", new JSONObject()
                    .put("major", "http://www.bsi.bund.de/ecard/api/1.1/resultmajor#error")
                    .put("minor", "http://www.bsi.bund.de/ecard/api/1.1/resultminor/al/common#internalError")
                    .put("language", "en")
                    .put("description", "An internal error has occurred during processing.")
                    .put("message", errorMsg)
            );
            result.put("url", "https://test.governikus-eid.de/gov_autent/async?refID=_abcdefgh");
            this.eidSession.close();
            this.eidSession.returnMessage("AUTH", result);
        } catch (JSONException e) {
            Log.e("AusweisMock", "Failed to generate JSON", e);
        }
    }

}
