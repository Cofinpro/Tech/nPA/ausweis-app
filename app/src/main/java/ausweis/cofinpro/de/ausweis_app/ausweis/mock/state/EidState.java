package ausweis.cofinpro.de.ausweis_app.ausweis.mock.state;

import android.os.RemoteException;


/**
 * @author Gregor Tudan, Cofinpro AG
 */
public interface EidState {

    default void initialize() throws RemoteException {
        throw new IllegalStateException();
    }

    default void setPIN(String pin) throws RemoteException {
        throw new IllegalStateException();
    }

    default void setPUK(String puk) throws RemoteException {
        throw new IllegalStateException();
    }

    default void setCAN(String can) throws RemoteException {
        throw new IllegalStateException();
    }

    default void receivedNFC() throws RemoteException {

    }

    default void accept() throws RemoteException {
        throw new IllegalStateException();
    }

}
