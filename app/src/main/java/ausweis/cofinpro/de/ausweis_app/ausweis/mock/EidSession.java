package ausweis.cofinpro.de.ausweis_app.ausweis.mock;

import android.nfc.Tag;
import android.os.RemoteException;
import android.util.Log;
import ausweis.cofinpro.de.ausweis_app.ausweis.mock.state.InitialState;
import ausweis.cofinpro.de.ausweis_app.ausweis.mock.state.EidState;
import com.governikus.ausweisapp2.IAusweisApp2SdkCallback;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * @author Gregor Tudan, Cofinpro AG
 */
public class EidSession {

    private Tag tag;
    private EidState eidState;
    private final IAusweisApp2SdkCallback callback;
    private boolean closed;

    EidSession(IAusweisApp2SdkCallback callback) throws RemoteException {
        this.callback = callback;
        this.eidState = new InitialState(this);
    }

    EidState getEidState() {
        return eidState;
    }

    public Tag getTag() {
        return tag;
    }

    void setTag(Tag tag) throws RemoteException {
        this.tag = tag;
        this.eidState.receivedNFC();
    }

    public void setEidState(EidState eidState) {
        this.eidState = eidState;
    }

    public void close() {
        this.closed = true;
    }

    public boolean isClosed() {
        return closed;
    }

    public void returnMessage(String msg, Map<String, Object> payload) throws RemoteException {
        JSONObject json = new JSONObject();
        try {
            if (payload != null) {
                for (Map.Entry<String, Object> entry : payload.entrySet()) {
                    json.put(entry.getKey(), entry.getValue());
                }
            }
            json.put("msg", msg);
            callback.receive(json.toString());
        } catch (JSONException e) {
            Log.e("AusweisMock", "Failed to generate json for reply", e);
        }
    }

    void initialize() throws RemoteException {
        this.eidState.initialize();
    }


}
