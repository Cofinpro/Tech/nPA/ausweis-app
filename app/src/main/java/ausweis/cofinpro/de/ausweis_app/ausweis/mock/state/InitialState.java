package ausweis.cofinpro.de.ausweis_app.ausweis.mock.state;

import android.os.RemoteException;
import android.util.Log;
import ausweis.cofinpro.de.ausweis_app.ausweis.mock.EidSession;
import org.json.JSONException;

import java.util.Collections;

/**
 * @author Gregor Tudan, Cofinpro AG
 */
public class InitialState implements EidState{
    private final EidSession eidSession;

    public InitialState(EidSession eidSession) {
        Log.v("AusweisMock", "Reached state " + getClass().getName());
        this.eidSession = eidSession;
    }

    @Override
    public void initialize() throws RemoteException {
        try {
            this.eidSession.returnMessage("AUTH", Collections.emptyMap());
            PermissionState eidState = new PermissionState(this.eidSession);
            this.eidSession.setEidState(eidState);
            eidState.requestPermissions();
        } catch (JSONException e) {
            Log.e("AusweisMock", "Failed to generate JSON", e);
        }
    }
}
