package ausweis.cofinpro.de.ausweis_app;

import android.content.Context;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Parcel;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import java.util.Collections;
import java.util.List;

import ausweis.cofinpro.de.ausweis_app.ausweis.AusweisClient;
import ausweis.cofinpro.de.ausweis_app.ausweis.Permission;
import ausweis.cofinpro.de.ausweis_app.ausweis.PermissionAdapter;

public class MainActivity extends AppCompatActivity {

    private AusweisClient ausweisClient;
    private ForegroundDispatcher mDispatcher;

    private Button ackknowledgePermissionsButton;
    private ListView permissionsList;

    private EditText pinField;
    private EditText canField;
    private EditText pukField;

    private TextView pinError;
    private TextView canError;
    private TextView pukError;

    private ViewFlipper vf;

    private TextView waitMessage;

    private List<Permission> requestedPermissions = Collections.emptyList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        vf = findViewById(R.id.vf);
        vf.setInAnimation(AnimationUtils.loadAnimation(this, R.anim.in_from_right));

        permissionsList = findViewById(R.id.permissionsListView);
        ackknowledgePermissionsButton = findViewById(R.id.buttonAcknowledgePermissions);
        ackknowledgePermissionsButton.setEnabled(false);

        pinField = this.findViewById(R.id.pinField);
        pinField.setOnEditorActionListener((view, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE || event.getKeyCode() == KeyEvent.KEYCODE_ENTER || event.getKeyCode() == KeyEvent.KEYCODE_NUMPAD_ENTER) {
                clickPin(view);
                return true;
            }
            return false;
        });

        canField = this.findViewById(R.id.canField);
        canField.setOnEditorActionListener((view, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE || event.getKeyCode() == KeyEvent.KEYCODE_ENTER || event.getKeyCode() == KeyEvent.KEYCODE_NUMPAD_ENTER) {
                clickCan(view);
                return true;
            }
            return false;
        });

        pukField = this.findViewById(R.id.pukField);
        pukField.setOnEditorActionListener((view, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE || event.getKeyCode() == KeyEvent.KEYCODE_ENTER || event.getKeyCode() == KeyEvent.KEYCODE_NUMPAD_ENTER) {
                clickPuk(view);
                return true;
            }
            return false;
        });

        pinError = this.findViewById(R.id.pinError);
        canError = this.findViewById(R.id.canError);
        pukError = this.findViewById(R.id.pukError);

        waitMessage = this.findViewById(R.id.waitMessage);

        mDispatcher = new ForegroundDispatcher(this);
        ausweisClient = new AusweisClient(this);
    }

    private boolean checkInstallationStatus() {
        boolean preconditions = ausweisClient.checkPreconditions();
        if (!preconditions) {
            new AusweisAppDialog().show(getSupportFragmentManager(), "ausweisApp");
        }
        return preconditions;
    }

    public void clickLegitimate(View view) {
        if (checkInstallationStatus()) {
            ausweisClient.createConnection();
            nextView();
        }
    }

    public void acceptPermissions(View view) {
        if (!this.requestedPermissions.isEmpty()) {
            for (Permission requestedPermission : this.requestedPermissions) {
                Log.i("ausweis", "Accepted " + requestedPermission);
            }
        }
        boolean accepted = ausweisClient.accept();
        if (accepted) {
            nextView();
        } else {
            Log.w("ausweis", "Not accepted!");
        }
    }

    // Just for mock testing in emulator
    public void clickMockNFC(View view) {
        Intent intent = new Intent();
        intent.putExtra(NfcAdapter.EXTRA_TAG, Tag.CREATOR.createFromParcel(Parcel.obtain()));
        ausweisClient.handleIntent(intent);
    }

    public void clickPin(View view) {
        InputMethodManager imm = (InputMethodManager) pinField.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(pinField.getWindowToken(), 0);
        String pin = pinField.getText().toString();
        Log.v("ausweis", "Entered PIN: " + pin);
        showWait("Deine PIN wird überprüft...");
        ausweisClient.setPIN(pin);
    }

    public void clickCan(View view) {
        InputMethodManager imm = (InputMethodManager) canField.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(canField.getWindowToken(), 0);
        String can = canField.getText().toString();
        Log.v("ausweis", "Entered CAN: " + can);
        showWait("Deine CAN wird überprüft...");
        ausweisClient.setCAN(can);
    }

    public void clickPuk(View view) {
        InputMethodManager imm = (InputMethodManager) pukField.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(pukField.getWindowToken(), 0);
        String puk = pukField.getText().toString();
        Log.v("ausweis", "Entered PUK: " + puk);
        showWait("Deine PUK wird überprüft...");
        ausweisClient.setPUK(puk);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.v("ausweis", "Received new intent " + intent.getAction());
        ausweisClient.handleIntent(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            this.checkInstallationStatus();
        }
    }

    public void clickSignature(View view) {
        Intent intent = new Intent(this, SigningActivity.class);
        this.startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        mDispatcher.enable();
    }

    @Override
    public void onPause() {
        super.onPause();
        mDispatcher.disable();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        flipBack();
    }

    private void flipBack() {
        int currentIndex = vf.indexOfChild(vf.getCurrentView());
        if (currentIndex > 0) {
            vf.showPrevious();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            flipBack();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    private void nextView() {
        vf.showNext();
    }

    public void showPinView(String errorMessage) {
        Log.v("ausweis", "navigating to PIN with error: " + errorMessage);
        pinField.setText(null);
        pinError.setText(errorMessage);
        if (vf.getDisplayedChild() != 3) {
            vf.setDisplayedChild(3);
        }
    }

    public void showCanView(String errorMessage) {
        Log.v("ausweis", "navigating to CAN with error: " + errorMessage);
        canField.setText(null);
        canError.setText(errorMessage);
        vf.setDisplayedChild(4);
    }

    public void showPukView(String errorMessage) {
        Log.v("ausweis", "navigating to PUK with error: " + errorMessage);
        pukField.setText(null);
        pukError.setText(errorMessage);
        vf.setDisplayedChild(5);
    }

    public void showSuccess() {
        Log.v("ausweis", "SUCCESS");
        this.vf.setDisplayedChild(6);
    }

    public void showError(String errorMessage) {
        Log.v("ausweis", "ERROR: " + errorMessage);
        this.vf.setDisplayedChild(7);
    }

    private void showWait(String message) {
        Log.v("ausweis", "navigating to WAIT with message: " + message);
        waitMessage.setText(message);
        vf.setDisplayedChild(8);
    }

    public void showPermissionsList(List<Permission> permissions) {
        if (!permissions.isEmpty()) {
            this.requestedPermissions = permissions;
            this.permissionsList.clearChoices();
            permissions.forEach(p -> Log.v("ausweis", "Permission requested: " + p.getName() + ", required=" + p.isRequired()));
            BaseAdapter permissionAdapter = new PermissionAdapter(this, permissions);
            this.permissionsList.setAdapter(permissionAdapter);
            this.ackknowledgePermissionsButton.setEnabled(true);
        } else {
            Log.w("ausweis", "No permissions requested");
        }
    }

}
