package ausweis.cofinpro.de.ausweis_app.ausweis;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ausweis.cofinpro.de.ausweis_app.R;

public class PermissionAdapter extends BaseAdapter {

    private final List<Permission> permissions;
    private final Activity context;

    private static final Map<String, Integer> permissionNames = new HashMap<>();
    private static final String FAMILY_NAME = "FamilyName";
    private static final String GIVEN_NAMES = "GivenNames";
    private static final String DOCUMENT_TYPE = "DocumentType";

    static {
        permissionNames.put(FAMILY_NAME, R.string.permission_familyname);
        permissionNames.put(GIVEN_NAMES, R.string.permission_givenname);
        permissionNames.put(DOCUMENT_TYPE, R.string.permission_documenttype);
    }

    public PermissionAdapter(Activity applicationContext, List<Permission> permissions) {
        this.permissions = permissions;
        this.context = applicationContext;
    }

    @Override
    public int getCount() {
        return permissions.size();
    }

    @Override
    public Object getItem(int position) {
        return permissions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return permissions.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_single, null, true);
        }

        Object item = getItem(position);
        if (item instanceof Permission) {
            Permission permission = (Permission) item;
            String pName = permission.getName() + " (%s)";
            Integer resId = permissionNames.get(permission.getName());
            if (resId != null) {
                pName = convertView.getResources().getString(resId);
            }
            int resRequired = permission.isRequired() ? R.string.required : R.string.optional;
            String requiredMessage = convertView.getResources().getString(resRequired);

            CheckedTextView checkedListItem = convertView.findViewById(R.id.listItemText);
            checkedListItem.setText(String.format(pName, requiredMessage));

            boolean checked = permission.isRequired();
            checkedListItem.setChecked(checked);
            if (checked) {
                checkedListItem.setCheckMarkDrawable(R.drawable.checked_disabled);
                checkedListItem.setEnabled(false);
            } else {
                checkedListItem.setCheckMarkDrawable(null);
            }

            checkedListItem.setOnClickListener(v -> {
                if (!permission.isRequired()) {
                    boolean switchedValue = !checkedListItem.isChecked();
                    Log.i("ausweis", "Switched "+  permission + " to " + switchedValue);
                    permission.setGranted(switchedValue);
                    checkedListItem.setChecked(switchedValue);
                    if (switchedValue) {
                        checkedListItem.setCheckMarkDrawable(R.drawable.checked);
                    } else {
                        checkedListItem.setCheckMarkDrawable(null);
                    }
                }
            });
        }
        return convertView;
    }


}
