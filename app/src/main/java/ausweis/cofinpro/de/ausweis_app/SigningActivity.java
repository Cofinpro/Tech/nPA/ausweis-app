package ausweis.cofinpro.de.ausweis_app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import com.github.gcacace.signaturepad.views.SignaturePad;

public class SigningActivity extends AppCompatActivity {

    private SignaturePad pad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signing);
        pad = findViewById(R.id.signature_pad);
    }

    public void reset(View view) {
        pad.clear();
    }

    public void submit(View view) {
        // Get as bitmap
        pad.getTransparentSignatureBitmap();

        Intent intent = new Intent(this, DoneActivity.class);
        this.startActivity(intent);
    }

}
