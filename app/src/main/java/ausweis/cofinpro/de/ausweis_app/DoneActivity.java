package ausweis.cofinpro.de.ausweis_app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class DoneActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_done);
    }

}
